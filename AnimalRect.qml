import QtQuick 2.15
import QtMultimedia

Item
{
    property string animal: " "
    property string img_src: " "
    property string sound_src: " "
    property int w: 0
    property int h: 0
    width: w
    height: h
    Rectangle
    {
        Behavior on rotation {
            NumberAnimation {
                duration: 5000
                easing.type: Easing.InOutQuad
            }
        }
        width: w
        height: h
        id:img_rect
        SoundEffect {
            id: sound_animal
            source: sound_src
        }
        Image {
            anchors.fill: parent
            source: img_src
            fillMode: Image.PreserveAspectFit
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    img_rect.rotation = 360
                    sound_animal.play()
                    print ("clicked on " + animal)
                    win.animals[win.cur_anim] == animal ? win.points++ : win.points--
                    win.cur_anim = (win.cur_anim+2)%10
                }
            }
        }
    }
}
