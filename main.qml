import QtQuick 2.15
import QtQuick.Window 2.15
import QtMultimedia
import QtQuick.Layouts 1.3

Window {
    id: win
    width: 600
    height: 400
    visible: true
    title: qsTr("Kids' captcha")
    function isRightAnim(anim)
    {
        return
    }
    function increasePoints()
    {
        points++;
        print("increased")
    }
    function reducePoints()
    {
        points--;
        print("reduced")
    }
    property int points : 0
    property int cur_anim : 0
    property int timer_started : 0
    property var animals: ["cat", "cat", "crow","crow", "duck", "duck", "frog", "frog", "pig", "pig",]
    property var sounds: ["qrc:/cat-meow4.wav","qrc:/cat-meow4.wav","qrc:/crow-1.wav", "qrc:/crow-1.wav", "duck-quack1.wav","duck-quack1.wav", "qrc:/frog-ribbet4.wav", "qrc:/frog-ribbet4.wav", "qrc:/pig-3.wav", "qrc:/pig-3.wav"]
    property var images: ["qrc:/cat.jpg", "qrc:/cat_2.jpg", "qrc:/craw_2.jpg", "qrc:/crow.jpg","qrc:/duck.jpg", "qrc:/duck_2.jpg", "qrc:/frog.jpeg", "qrc:/frog_2.jpg", "qrc:/pig.jpg", "qrc:/pig_2.jpg"]
    Rectangle {
        id:win_rect
        width: win.width*0.96
        height: win.height*0.96
        anchors.centerIn: parent
        color: "black"
        Column {
            anchors.fill: parent
            Rectangle {
                id: top_rect
                width: parent.width
                height: win_rect.height*0.25
                color: "black"
                Column {
                    anchors.fill: parent
                    Rectangle {
                        width: parent.width
                        height: top_rect.height*0.4
                        Text { text: "Wellcome to kids' captcha!"; font.bold: true; anchors.horizontalCenter: parent.horizontalCenter}
                    }
                    Rectangle {
                        anchors.right: parent.right
                        width: parent.width/3
                        height: top_rect.height*0.2
                        Text {
                            text: "Points: " + win.points;
                            font.bold: true;
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                    }
                    Rectangle {
                        width: parent.width
                        height: top_rect.height*0.3
                        Text {
                            text: "Find " + animals[cur_anim];
                            font.bold: true;
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                    }
                    Rectangle {
                        width: parent.width
                        height: parent.height*0.05
                        color: "black"
                    }
                }
            }
            Rectangle {
                anchors.horizontalCenter: parent.horizontalCenter
                id: bottom_rect
                width: height
                height: win_rect.height - top_rect.height
                color: "black"
                Grid {
                    anchors.fill: parent
                    spacing:0
                    columns: 3
                    Repeater{
                        model:9
                        AnimalRect {
                            img_src: win.images[index]
                            sound_src: win.sounds[index]
                            animal: win.animals[index]
                            w: bottom_rect.width/3
                            h: bottom_rect.height/3
                        }
                    }
                }
            }
            Rectangle {
                width: parent.width
                height: parent.height*0.05
                color: "black"
            }
       }
    }
}
