# QML_multimedia

## Description

This project is an assignment in QML course. 
The assignment instructions: 
- You are create part of the game for kids. 
- There is a picture and kid have animals hiding on the picture. 
- Use at least 5 different animals.
- When you click the animal, it is producing the sound and start to animate. 
- Add points for each found animal.

The resulting app view:\
![screen](pics/general.jpg)\
The black area before points was supposed to be timer animation. But in the end, the timer wasn't implemented.

When click on image happens, points update and rotation animation is done:  
![screen](pics/rotation.jpg)

## Usage

- install [Qt](https://www.qt.io/download-qt-installer)
- clone the repository, and open it in Qt Creator.
- Build and run the application using Qt Creator GUI.

## Maintainers
@MariaNema
